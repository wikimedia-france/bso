from pathlib import Path
import xml.etree.ElementTree as ET
import bz2
import time

DATA_RAW = Path("data/parts/dumps")
DATA_RAW.mkdir(parents=True, exist_ok=True)

DATA_PROCESSED = Path("data/parts/dumps/chunks/")
DATA_PROCESSED.mkdir(parents=True, exist_ok=True)

wikipedia_history_archive = (
    DATA_RAW / "frwiki-20240801-pages-meta-history1.xml-p1p1209.bz2"
)


# Function to process XML data from a chunk
def process_xml_page_revisions(page_data):

    # parse the page
    page = ET.fromstring(page_data)

    contributors = set()

    # loop on revisions
    nb_revisions = 0
    for revision in page.findall("revision"):
        # get the contributor username
        contributor = revision.find("contributor")
        username = contributor.find("username")
        if username is not None:
            contributors.add(username.text)
        nb_revisions += 1

    # print(f"Page {page.find('title').text} has {nb_revisions} revisions")

    return page.find("title").text


def split_and_process_pages(input_file, output_path, chunk_size=10, chunk_nb_page=200):
    output_path = Path(output_path)
    output_path.mkdir(parents=True, exist_ok=True)

    start_inter_exec = time.time()

    with bz2.open(input_file, "rt", encoding="utf-8") as f:
        buffer_data = ""
        chunk_number = 1
        pages_accumulated = 0
        chunk_data = ""

        end_of_file = False
        while not end_of_file:
            buffer_data += f.read(chunk_size * 1024 * 1024)  # Read in MB chunks

            if not buffer_data:
                end_of_file = True
                break

            while True:
                start_page = buffer_data.find("<page>")
                if start_page == -1:
                    # No more pages found, keep remaining data in buffer and break
                    buffer_data = buffer_data[:]
                    break

                end_page = buffer_data.find("</page>") + len("</page>")
                page_data = buffer_data[start_page:end_page]

                chunk_data += page_data
                pages_accumulated += 1

                if pages_accumulated % chunk_nb_page == 0:
                    # Write chunk to file
                    output_file = (
                        output_path / f"{input_file.stem}_chunk{chunk_number}.xml"
                    )
                    with open(output_file, "w", encoding="utf-8") as output:
                        output.write(chunk_data)

                    print(f"Chunk {chunk_number} with {chunk_nb_page} pages written")

                    chunk_number += 1
                    chunk_data = ""
                    pages_accumulated = 0

                # Remove the processed page from buffer_data
                buffer_data = buffer_data[end_page:]

            # Write any remaining pages in buffer if needed
            if pages_accumulated > 0:
                output_file = output_path / f"{input_file.stem}_chunk{chunk_number}.xml"
                with open(output_file, "w", encoding="utf-8") as output:
                    output.write(chunk_data)

                print(f"Final chunk {chunk_number} with remaining pages written")

            print(f"Execution time: {time.time() - start_inter_exec:.2f} seconds")


start_exec = time.time()

split_and_process_pages(
    wikipedia_history_archive,
    DATA_PROCESSED,
    chunk_size=10,
    chunk_nb_page=200,
)

print()
print(f"Execution time: {time.time() - start_exec:.2f} seconds")
