import bz2
from lxml import etree

def split_xml_bz2(input_file, output_prefix, chunk_size=10000):
    # Open the bz2 file
    with bz2.open(input_file, 'rb') as bz2_file:

        print("yo")

        # Create an XML parser
        parser = etree.XMLParser(remove_blank_text=True)
        
        # Create an iterparse object without passing the parser to iterparse
        context = etree.iterparse(bz2_file, events=("end",), tag="page")
        
        chunk = []
        chunk_num = 1

        for event, elem in context:
            chunk.append(etree.tostring(elem))
            if len(chunk) >= chunk_size:
                with open(f"{output_prefix}_part{chunk_num}.xml", "wb") as f:
                    f.write(b"<?xml version='1.0' encoding='UTF-8'?>\n<pages>\n")
                    f.writelines(chunk)
                    f.write(b"</pages>")
                chunk = []
                chunk_num += 1
            elem.clear()
        
        if chunk:
            with open(f"{output_prefix}_part{chunk_num}.xml", "wb") as f:
                f.write(b"<?xml version='1.0' encoding='UTF-8'?>\n<pages>\n")
                f.writelines(chunk)
                f.write(b"</pages>")

split_xml_bz2("data/parts/dumps/frwiki-20240801-pages-meta-history1.xml-p1p1209.bz2", "wikipedia_chunk")
