# -*- encoding: utf-8 -*-

# taken from https://en.wikipedia.org/wiki/Help:citation_Style_1
citation_template_names = set([
    'citation',
    'cite arxiv',
    'cite av media',
    'cite av media notes',
    'cite book',
    'cite conference',
    'cite encyclopedia',
    'cite episode',
    'cite interview',
    'cite journal',
    'cite magazine',
    'cite mailing list',
    'cite map',
    'cite news',
    'cite newsgroup',
    'cite podcast',
    'cite press release',
    'cite report',
    'cite serial',
    'cite sign',
    'cite speech',
    'cite tech report',
    'cite thesis',
    'cite web',
    'wayback',
    'webcite',
    'memento',
    'webarchive'
])
