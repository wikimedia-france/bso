# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import unittest
import mwparserfromhell
from wikiciteparser.parser import *

# from translator import translate_and_parse_citation_template


class ParsingTests(unittest.TestCase):
    @unittest.skip("Not relevant for multi-language parsing")
    def test_multiple_authors(self):
        p = parse_citation_dict({"doi": "10.1111/j.1365-2486.2008.01559.x",
                                 "title": "Climate change, plant migration, and range collapse in a global biodiversity hotspot: the ''Banksia'' (Proteaceae) of Western Australia",
                                 "issue": "6",
                                 "journal": "Global Change Biology",
                                 "year": "2008",
                                 "volume": "14",
                                 "last4": "Dunn",
                                 "last1": "Fitzpatrick",
                                 "last3": "Sanders",
                                 "last2": "Gove", "first1":
                                 "Matthew C.",
                                 "first2": "Aaron D.",
                                 "first3": "Nathan J.",
                                 "first4": "Robert R.",
                                 "pages": "1\u201316"
                                 },
                                template_name='cite journal')
        self.assertEqual(p['Authors'], [{'last': 'Fitzpatrick',
                                         'first': 'Matthew C.'
                                         },
                                        {'last': 'Gove',
                                         'first': 'Aaron D.'},
                                        {'last': 'Sanders',
                                         'first': 'Nathan J.'},
                                        {'last': 'Dunn',
                                         'first': 'Robert R.'
                                         }
                                        ])

    @unittest.skip("Not relevant for multi-language parsing")
    def test_vauthors(self):
        p = parse_citation_dict({"doi": "10.1016/s1097-2765(00)80111-2",
                                 "title": "SAP30, a component of the mSin3 corepressor complex involved in N-CoR-mediated repression by specific transcription factors",
                                 "journal": "Mol. Cell",
                                 "volume": "2",
                                 "date": "July 1998",
                                 "pmid": "9702189",
                                 "issue": "1",
                                 "pages": "33\u201342",
                                 "vauthors": "Laherty CD, Billin AN, Lavinsky RM, Yochum GS, Bush AC, Sun JM, Mullen TM, Davie JR, Rose DW, Glass CK, Rosenfeld MG, Ayer DE, Eisenman RN"
                                 },
                                template_name='cite journal')
        self.assertEqual(p['Authors'], [{'last': 'Laherty',
                                         'first': 'CD'
                                         },
                                        {'last': 'Billin',
                                         'first': 'AN'
                                         },
                                        {'last': 'Lavinsky',
                                         'first': 'RM'
                                         },
                                        {'last': 'Yochum',
                                         'first': 'GS'
                                         },
                                        {'last': 'Bush',
                                         'first': 'AC'
                                         },
                                        {'last': 'Sun',
                                         'first': 'JM'
                                         },
                                        {'last': 'Mullen',
                                         'first': 'TM'
                                         },
                                        {'last': 'Davie',
                                         'first': 'JR'
                                         },
                                        {'last': 'Rose',
                                         'first': 'DW'
                                         },
                                        {'last': 'Glass',
                                         'first': 'CK'
                                         },
                                        {'last': 'Rosenfeld',
                                         'first': 'MG'
                                         },
                                        {'last': 'Ayer',
                                         'first': 'DE'
                                         },
                                        {'last': 'Eisenman',
                                         'first': 'RN'
                                         }
                                        ])

    @unittest.skip("Not relevant for multi-language parsing")
    def test_remove_links(self):
        p = parse_citation_dict({"title": "Mobile, Alabama",
                                 "url": "http://archive.org/stream/ballouspictorial1112ball#page/408/mode/2up",
                                 "journal": "[[Ballou's Pictorial Drawing-Room Companion]]",
                                 "volume": "12",
                                 "location": "Boston",
                                 "date": "June 27, 1857"
                                 },
                                template_name='cite journal')
        self.assertEqual(p['Periodical'],
                         "Ballou's Pictorial Drawing-Room Companion")

    @unittest.skip("Not relevant for multi-language parsing")
    def test_authorlink(self):
        p = parse_citation_dict({"publisher": "[[World Bank]]",
                                 "isbn": "978-0821369418",
                                 "title": "Performance Accountability and Combating Corruption",
                                 "url": "http://siteresources.worldbank.org/INTWBIGOVANTCOR/Resources/DisruptingCorruption.pdf",
                                 "page": "309",
                                 "last1": "Shah",
                                 "location": "[[Washington, D.C.]], [[United States|U.S.]]",
                                 "year": "2007",
                                 "first1": "Anwar",
                                 "authorlink1": "Anwar Shah",
                                 "oclc": "77116846"
                                 },
                                template_name='citation')
        self.assertEqual(p['Authors'], [{'link': 'Anwar Shah',
                                         'last': 'Shah',
                                         'first': 'Anwar'
                                         }
                                        ])

    @unittest.skip("Not relevant for multi-language parsing")
    def test_unicode(self):
        p = parse_citation_dict({"title": "\u0414\u043e\u0440\u043e\u0433\u0438 \u0446\u0430\u0440\u0435\u0439 (Roads of Emperors)",
                                 "url": "http://magazines.russ.ru/ural/2004/10/mar11.html",
                                 "journal": "\u0423\u0440\u0430\u043b",
                                 "author": "Margovenko, A",
                                 "volume": "10",
                                 "year": "2004"
                                 },
                                template_name='cite journal')
        self.assertEqual(p['Title'],
                         '\u0414\u043e\u0440\u043e\u0433\u0438 \u0446\u0430\u0440\u0435\u0439 (Roads of Emperors)')

    def test_mwtext_en(self):
        # taken from https://en.wikipedia.org/wiki/Joachim_Lambek
        mwtext = """
        ===Articles===
        * {{Citation | last1=Lambek | first1=Joachim | author1-link=Joachim Lambek | last2=Moser | first2=L. | title=Inverse and Complementary Sequences of Natural Numbers| doi=10.2307/2308078 | mr=0062777  | journal=[[American Mathematical Monthly|The American Mathematical Monthly]] | issn=0002-9890 | volume=61 | issue=7 | pages=454–458 | year=1954 | jstor=2308078 | publisher=The American Mathematical Monthly, Vol. 61, No. 7}}
        * {{Citation | last1=Lambek | first1=J. | author1-link=Joachim Lambek | title=The Mathematics of Sentence Structure | year=1958 | journal=[[American Mathematical Monthly|The American Mathematical Monthly]] | issn=0002-9890 | volume=65 | pages=154–170 | doi=10.2307/2310058 | issue=3 | publisher=The American Mathematical Monthly, Vol. 65, No. 3 | jstor=1480361}}
        *{{Citation | last1=Lambek | first1=Joachim | author1-link=Joachim Lambek | title=Bicommutators of nice injectives | doi=10.1016/0021-8693(72)90034-8 | mr=0301052  | year=1972 | journal=Journal of Algebra | issn=0021-8693 | volume=21 | pages=60–73}}
        *{{Citation | last1=Lambek | first1=Joachim | author1-link=Joachim Lambek | title=Localization and completion | doi=10.1016/0022-4049(72)90011-4 | mr=0320047  | year=1972 | journal=Journal of Pure and Applied Algebra | issn=0022-4049 | volume=2 | pages=343–370 | issue=4}}
        *{{Citation | last1=Lambek | first1=Joachim | author1-link=Joachim Lambek | title=A mathematician looks at Latin conjugation | mr=589163  | year=1979 | journal=Theoretical Linguistics | issn=0301-4428 | volume=6 | issue=2 | pages=221–234 | doi=10.1515/thli.1979.6.1-3.221}}
        {{cite journal | vauthors = Caboche M, Bachellerie JP | title = RNA methylation and control of eukaryotic RNA biosynthesis. Effects of cycloleucine, a specific inhibitor of methylation, on ribosomal RNA maturation | journal = European Journal of Biochemistry | volume = 74 | issue = 1 | pages = 19–29 | date = March 1977 | pmid = 856572 | doi = 10.1111/j.1432-1033.1977.tb11362.x | doi-access = free }}
        {{Wayback|url=http://www.mondonedoferrol.org/estudios-mindonienses/MINDONIENSES%2024%20&#91;protegido&#93;.pdf |date=20160611035306 }}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'en')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'pmid' in tpl.lower():
                self.assertIn('ID_list', parsed)

    # Multi-language citation extraction
    def test_translate_ca(self):
        mwtext = """
            {{ref-notícia|url=http://24.sapo.pt/noticias/internacional/artigo/musico-dany-silva-destaca-incremento-das-relacoes-culturais-entre-cabo-verde-e-portugal_19090155.html|títol=Músico Dany Silva destaca incremento das relações culturais entre Cabo Verde e Portugal|agència=Lusa|data=11 abril 2015|consulta=17 juliol 2016|citació="O futuro da música de Cabo Verde é risonho. Está indo cada vez mais com mais força e aceitação por esse mundo fora", avaliou Dany Silva, 68 anos|llengua=portuguès|arxiuurl=https://web.archive.org/web/20161005124852/http://24.sapo.pt/noticias/internacional/artigo/musico-dany-silva-destaca-incremento-das-relacoes-culturais-entre-cabo-verde-e-portugal_19090155.html|arxiudata=5 d’octubre 2016}}
            {{Webarchive|url=https://web.archive.org/web/20161005124852/http://24.sapo.pt/noticias/internacional/artigo/musico-dany-silva-destaca-incremento-das-relacoes-culturais-entre-cabo-verde-e-portugal_19090155.html |date=5 d’octubre 2016 }}
            {{Ref-web |url=http://24.sapo.pt/noticias/internacional/artigo/musico-dany-silva-destaca-incremento-das-relacoes-culturais-entre-cabo-verde-e-portugal_19090155.html |títol=Còpia arxivada |consulta=2016-07-17 |arxiuurl=https://web.archive.org/web/20161005124852/http://24.sapo.pt/noticias/internacional/artigo/musico-dany-silva-destaca-incremento-das-relacoes-culturais-entre-cabo-verde-e-portugal_19090155.html |arxiudata=2016-10-05 }}
            {{ref-web|url=http://www.danysilva.com/bio-port.shtml|títol=Biografia de Dany Silva|llengua=portuguès|consulta=2016-07-17|arxiuurl=https://web.archive.org/web/20131105194138/http://www.danysilva.com/bio-port.shtml|arxiudata=2013-11-05}}
            {{ref-publicació |cognom=Bofarull i Terrades |nom=Manuel|títol=Jaume Ramon i Vidales, un historiador del Penedés|publicació= Miscel·lània penedesenca |data=1987 |pàgines= 189-222|volum=Vol.: 10|url=http://www.raco.cat/index.php/MiscellaniaPenedesenca/article/view/59275/91814 |consulta=17 juliol 2016}}
            {{ref-llibre |cognom=Becat |nom=Joan |cognom2=Ponsich |nom2=Pere |cognom3=Gual |nom3=Raimon |capítol=Sant Marçal |títol=El Rosselló i la Fenolleda | editorial=Fundació Enciclopèdia Catalana |lloc=Barcelona |data=1985 |col·lecció=Gran Geografia Comarcal de Catalunya, 14 |isbn=84-85194-59-4}}
            {{citar ref |autor1=— | autor2=Sákovics, J.|títol=Olson vs. Coase coalitional worth in conflict|lloc= Barcelona | editor=Universitat Pompeu Fabra |any=2002|llengua=anglès| oclc=807899208}}
            {{citar ref |url=http://www.elperiodicomediterraneo.com/noticias/opinion/julio-alcaide_87342.html |títol=Julio Alcaide |publicació=El Periódico del Mediterráneo |data=15 de gener de 2004 |consulta=2 d'octubre de 2015}}
            {{Llista d'episodis\n | EpisodiNúmero = 9\n | EpisodiNúmero2 = 9\n | Títol = '''Cheaters Never Prosper'''<br />Els delators mai prosperen\n | DirigitPer = Stacey K. Black\n | EscritPer = Mike Berchem\n | DataEmissióOriginal = [[8 d'octubre]] de [[2012]]\n | ResumCurt = L'equip investiga l'assassinat a [[Las Vegas]] d'un oficial de policia, que ha estat trobat mort en un lavabo per homes. Les sospites de la capitana Raydor de la irresponsabilitat del pare biològic d'en Rusty es confirmen com a certes.\n | ColorLínia = 1E90FF\n | Aux4 = 4,33}}
            {{Llista d'episodis\n | EpisodiNúmero = 52\n | EpisodiNúmero2 = 4\n | Títol = '''Turn Down'''<br />Rebutjar\n | DirigitPer = Paul McCrane\n | EscritPer = Adam Belanoff\n | DataEmissióOriginal = [[29 de juny]] de [[2015]]\n | ResumCurt = \n | ColorLínia = FF3030\n | Aux4 = 4,39}}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'ca')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'isbn' in tpl:
                self.assertIn('ID_list', parsed)

    def test_translate_da(self):
        mwtext = """
            {{Kilde www|titel=Shaoping Bai|url=https://vbn.aau.dk/da/persons/113766}}
            {{kilde| url = https://orienteering.sport/mtbo/events-and-results/| titel = MTB Orienteering. Events and Results| udgiver = eventor.orienteering.org | efternavn = IOF Eventor}}
            {{Kilde nyheder |title=Fra amatørklub til verdenseliten på halvandet år: Oprykkerne fra HB Køge skal spille Champions League |url=https://www.dr.dk/sporten/fodbold/kvindeligaen/fra-amatoerklub-til-verdenseliten-paa-halvandet-aar-oprykkerne-fra-hb |publisher=[[DR]] |date=17. maj 2021 |accessdate=20. juni 2021}}
            {{Kilde bog|efternavn=Blamey|fornavn=M.|efternavn2=Fitter|fornavn2=R.|år=2003|titel=Wild flowers of Britain and Ireland: The Complete Guide to the British and Irish Flora.|udgiver=A & C Black|sted=London|isbn=978-1408179505}}
            {{Kilde bog|fornavn=John W.|efternavn=Birkenmeier|titel=The Development of the Komnenian Army: 1081–1180|udgiver=Brill|år=2002|isbn=90-04-11710-5}}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'da')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'isbn' in tpl:
                self.assertIn('ID_list', parsed)

    def test_translate_de(self):
        mwtext = """
        {{Internetquelle|url=http://profootballtalk.nbcsports.com/2016/01/20/morgan-cox-jon-weeks-added-to-pro-bowl-as-long-snappers/|titel=Morgan Cox, Jon Weeks added to Pro Bowl as long snappers|datum=2016-01-20|autor=Josh Alper|sprache=en|zugriff=2016-01-28}}
        {{Literatur|Autor=Smith, C. N., Wixted, J. T. & Squire, L. R.|Titel=The hippocampus supports both recollection and familiarity when memories are strong. Journal of Neuroscience, 31|Jahr=2011|Seiten=15693–15702.}}
        {{cite web|url=http://www.cincinnati.com/story/sports/soccer/fc-cincinnati/2016/05/14/mclaughlins-goal-gives-fc-cincy-1-0-lead-half/84382158/|title=Another record crowd turns out to watch FC Cincy win|last=Brennan|first=Patrick|date=2016-05-14|work=[[Cincinnati Enquirer]]|accessdate=2016-10-07}}
        {{Webarchiv|url=http://www.kath-dekanat-tbb.de/html/seelsorgeeinheiten.html |wayback=20190712173456 |text=''Seelsorgeeinheiten des Dekanats Tauberbischofsheim'' |archiv-bot=2023-01-13 18:13:18 InternetArchiveBot }}
        {{Webarchiv|url=http://www.vertebrates.si.edu/msw/mswcfapp/msw/taxon_browser.cfm?msw_id=5398 |wayback=20151223190334 |text=Berylmys manipulus |archiv-bot=2019-04-29 10:19:15 InternetArchiveBot }}
            """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'de')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})

    def test_translate_es(self):
        mwtext = """
            {{cita publicación |apellidos=Thorsett, S.E.; Benjamin, R.A.; Brisken, Walter F.; Golden, A.; Goss, W.M.|año= 2003|título= Pulsar PSR B0656+14, the Monogem Ring, and the Origin of the Knee in the Primary Cosmic-Ray Spectrum |publicación=[[The Astrophysical Journal]] |volumen= 592|número= 2|páginas= L71-L73|ubicación= |editorial= |issn= |url= https://ui.adsabs.harvard.edu/abs/2003ApJ...592L..71T/abstract|fechaacceso=26 de septiembre de 2021}}
            {{cite journal |title = Planetary companions in K giants β Cancri, μ Leonis, and β Ursae Minoris|author=Lee, B.-C.|author2=Han, I.|author3=Park, M.-G.|author4=Mkrtichian, D. E.|author5=Hatzes, A. P.|author6=Kim, K.-M. |journal = [[Astronomy and Astrophysics]]\n|date = 2014 |volume = 566 |pages=A67 |doi = 10.1051/0004-6361/201322608 |bibcode = 2014A&A...566A..67L |arxiv = 1405.2127|s2cid=118631934}}
            {{cita web |url=http://www.fayerwayer.com/2011/09/armen-sus-maletas-descubren-el-exoplaneta-habitable-mas-similar-a-la-tierra/ |título=Armen sus maletas: Descubren el exoplaneta habitable más similar a la Tierra |editorial=Fayerwayer |fechaacceso= 4 de septiembre de 2011}}
            {{Cita web |título=Quilmes, la historia de una empresa que se vendió en 1800 millones de dólares|editorial=Infobae |url=http://www.infobae.com/2006/04/15/249296-quilmes-la-historia-una-empresa-que-se-vendio-us1800-millones |fechaacceso=25 de enero de 2016}}
            {{cita libro |apellidos= [[Gonzalo Martínez Diez|Martínez Díez]] |nombre= Gonzalo |coautores= |editor= Editorial Aldecoa |otros= |título= Colección Documental Del Monasterio de San Pedro de Cardeña |edición= |año= 1998 |editorial= Caja de Ahorros y Monte de Piedad del Círculo Católico de Obreros de Burgos |número= |idioma= es |id= |isbn=978-84-7009-567-2|página= |cita= }}
            {{cita publicación |apellidos=Knies, Jonathan R.; Sasaki, Manami; Plucinsky, Paul P. |año= 2018|título=Suzaku observations of the Monogem Ring and the origin of the Gemini H α ring  |publicación=[[Monthly Notices of the Royal Astronomical Society]] |volumen=477 |número= 4|páginas= 4414-4422|ubicación= |editorial= |issn= |url= https://ui.adsabs.harvard.edu/abs/2018MNRAS.477.4414K/abstract |fechaacceso=26 de septiembre de 2021}}
            {{Wayback|url=http://www.mondonedoferrol.org/estudios-mindonienses/MINDONIENSES%2024%20&#91;protegido&#93;.pdf |date=20160611035306 }}
            {{Cita noticia|apellidos=|nombre=Municipio B|título=MERCADO DEL INMIGRANTE|url=https://municipiob.montevideo.gub.uy/mercado-del-inmigrante|fecha=20 de septiembre de 2019|fechaacceso=|periódico=|ubicación=|página=|número=|urlarchivo=|fechaarchivo=}}
            {{Cita libro|apellidos=Rita Laima|título=Skylarks and Rebels: A Memoir about the Soviet Russian Occupation of Latvia, Life in a Totalitarian State, and Freedom|url=https://books.google.com/books?id=20UwDwAAQBAJ&q=Egl%C4%ABtis+An%C5%A1lavs+novelist&pg=PT319|año=2017|editorial=Columbia University Press|isbn=978-3-838-26854-5}}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'es')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'isbn' in tpl:
                self.assertIn('ID_list', parsed)

    def test_translate_fi(self):
        mwtext = """
            {{Kirjaviite | Tekijä = Modoc Press| Nimeke = Teacher education programs in the United States: a guide| Kappale = | Sivu = 50| Selite = | Julkaisija = Greenwood Publishing Group | Vuosi = 2004| Tunniste = ISBN 9780275981563 | www = http://books.google.com/books?id=hr8euMZorl4C&printsec=frontcover&source=gbs_v2_summary_r&cad=0#v=onepage&q=&f=false| www-teksti = Google Book| Viitattu = }}
            {{Kirjaviite | Nimeke = Thirty Years of Psychical Research | Vuosi = 1923 | Julkaisupaikka = New York | Julkaisija = The Macmillan Company | www = https://archive.org/details/30YearsRichet | www-teksti = Teoksen verkkoversio }}
            {{Lehtiviite | Tekijä = Hannu Autto | Otsikko = Elinor Ostrom ja yhteisresurssien monitieteellinen tutkimus  | Julkaisu = Kansantaloudellinen aikakauskirja | Vuosikerta = 106(1) | Sivut = 99–103 | Ajankohta = 2010 | www= http://www.taloustieteellinenyhdistys.fi/images/stories/kak/kak12010/kak12010autto.pdf}}
            {{Verkkoviite|osoite=https://www.filmifin.com/content/brasilian-pojat|nimeke=Brasilian pojat|tekijä=Jari Tomppo|julkaisu=FilmiFin|ajankohta=24.1.2005|julkaisija=FilmiFIN|viitattu=19.2.2023}}
            {{Kirjaviite | Nimeke=Poppareita, sankareita ja kaunottaria. [[Olavi Kaskisuo]] ja Suomen julkkishistorian villit vuodet | Julkaisupaikka=Helsinki | Julkaisija=Minerva | Vuosi=2006 | Tunniste=ISBN 952-5591-85-9}}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'fi')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'isbn' in tpl.lower():
                self.assertIn('ID_list', parsed)

    def test_translate_fr(self):
        mwtext = """
            {{Article|auteur1=Walter Moser|titre=« Puissance baroque » dans les nouveaux médias. À propos de Prospero’s Books de Peter Greenaway|périodique=Cinémas|volume=10|numéro=2-3|date=printemps 2000|lire en ligne=https://doi.org/10.7202/024815ar|pages=39–63}}
            {{article|prénom1=|nom1=|url=http://www.journaldesfemmes.com/loisirs/cinema/1740005-palmares-globes-de-cristal-2017-ceremonie/|titre=Globes de Cristal 2017 : le palmarès complet|consulté le=31 janvier 17|périodique=Le Journal des femmes|jour=31|mois=janvier|année=2017}}
            {{lien web|url=https://news.google.com/newspapers?nid=1310&dat=19580426&id=sMgUAAAAIBAJ&pg=3303,4437749|titre=Little America Will Float Away on an Iceberg|site=Eugene Register-Guard|date=April 1958|consulté le=14 décembre 2009}}
            {{article|titre=Mythopoïèse et souffrance familiale|auteur=Evelyn Granjon|année=2000|pages=13-22|périodique=Le Divan familial|numéro=4}}
            {{Ouvrage|auteur1=Sous la direction d'Emmanuelle Brugerolles|titre=Rembrandt et son entourage, Carnets d'études 23|passage=p. 74-77, Cat. 18|éditeur=Beaux-arts de Paris les éditions|date=2012-2014}}
            {{Ouvrage|auteur1=Emile Wiriot|titre=Le quartier Saint-Jacques et les quartiers voisins|éditeur=Tolra|année=1930|passage=254|lire en ligne=https://gallica.bnf.fr/ark:/12148/bpt6k165711w/f254.image}}
            {{Ouvrage|langue=es|prénom1=Pablo|nom1=Mendelevich|titre=El Relato Kirchnerista en 200 expresiones|éditeur=Ediciones B|lieu=Argentina|année=2013|pages=44-45|isbn=978-987-62-7412-8}}
            """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'fr')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'isbn' in tpl.lower():
                self.assertIn('ID_list', parsed)

    def test_translate_it(self):
        mwtext = """
                {{cita libro|autore=Gernot Wilhelm|titolo=The Hurrians|url=https://archive.org/details/hurrians0000wilh|editore=Aris & Philips Warminster|anno=1989|ISBN=0-85668-489-9}}
                {{cita libro | autore = Barth, F. | titolo = Ethnic groups and boundaries: The social organization of culture differences | url = https://archive.org/details/ethnicgroupsboun0000unse | lingua = en | editore = Little Brown & Co. | città = Boston | anno = 1969}}
                {{Cita pubblicazione|cognome=Kennedy |nome=Edward S. |data=1962 |titolo=Review: ''The Observatory in Islam and Its Place in the General History of the Observatory'' by Aydin Sayili |rivista=[[Isis (periodico)|Isis]] |volume=53 |numero=2 |pp=237-239 |doi=10.1086/349558 }}
                {{cita libro\n | autore = F. H. Shu\n | titolo = The Physical Universe\n | pubblicazione = University Science Books\n | data = 1982\n | città = Mill Valley, California\n | isbn = 0-935702-05-9}}
                {{cita web\n |titolo = Penn State Erie-School of Science-Astronomy and Astrophysics\n |url = http://www.erie.psu.edu/academic/science/degrees/astronomy/astrophysics.htm\n |accesso      = 20 giugno 2007\n |urlmorto     = sì\n |urlarchivio  = https://web.archive.org/web/20071101100832/http://www.erie.psu.edu/academic/science/degrees/astronomy/astrophysics.htm\n |dataarchivio = 1º novembre 2007\n}}
                {{cite web|first1=Peter|last1=Caranicas|access-date=November 7, 2018|title=ILM Launches TV Unit to Serve Episodic and Streaming Content|url=https://variety.com/2018/artisans/news/george-lucas-star-wars-ilm-launches-tv-unit-1203022007/|website=Variety|date=November 7, 2018}}
                {{cita libro|cognome= Vicario|nome= Salvatore G.|titolo= Fonte Nuova entra nella storia|annooriginale= 2004|editore= Libreria dello Stato, [[Istituto Poligrafico e Zecca dello Stato]]|città= Roma|ISBN= 88-240-1202-7|p= 118|capitolo= Museo d'Arte Contemporanea}}
                {{cita libro|cognome=Pablo|nome=Mendelevich|titolo=El Relato Kirchnerista en 200 expresiones|editore=Ediciones B|anno=2013|pages=44-45|isbn=978-987-62-7412-8}}
                """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'it')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'isbn' in tpl.lower():
                self.assertIn('ID_list', parsed)

    def test_translate_nl(self):
        mwtext = """
                {{Citeer web|url=https://eurovision.tv/about/rules|titel=The Rules of the Contest|taal=en|werk=Eurovision.tv|archiefurl=https://web.archive.org/web/20220716003114/https://eurovision.tv/about/rules|archiefdatum=16 juli 2022}}
                {{Citeer boek | achternaam = D Hillenius ea | titel = Spectrum Dieren Encyclopedie Deel 7 | pagina's = Pagina 2312 | datum = 1971 | uitgever = Uitgeverij Het Spectrum| ISBN = 90 274 2097 1}}
                {{citeer journal | auteur = Higgs PG | title = RNA secondary structure: physical and computational aspects | url = https://archive.org/details/sim_quarterly-reviews-of-biophysics_2000-08_33_3/page/199 | journal = Quarterly Reviews of Biophysics | volume = 33 | issue = 3 | pages = 199–253 | date = 2000 | pmid = 11191843 | doi = 10.1017/S0033583500003620 }}
                {{Citeer web|url=https://charts.org.nz/showitem.asp?interpret=Eric+Clapton&titel=461+Ocean+Boulevard&cat=a|
                    =https://web.archive.org/web/20121102111730/https://charts.org.nz/showitem.asp?interpret=Eric+Clapton&titel=461+Ocean+Boulevard&cat=a
                    |title=Eric Clapton - 461 Ocean Boulevard,
                    |dodelink=ja,
                    |work=charts.org.nz,
                    |bezochtdatum=18 augustus 2012,
                    |archive-date=2 november 2012,
                    |archive-url=https://web.archive.org/web/20121102111730/https://charts.org.nz/showitem.asp?interpret=Eric+Clapton&titel=461+Ocean+Boulevard&cat=a
                }}
                """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'nl')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})

    def test_translate_no(self):
        mwtext = """
            {{Kilde www |url=http://www.akershus.no/om-fylkeskommunen/om-akershus/kommuner-og-regioner/ |tittel=Akershus fylkeskommune |besøksdato=2018-06-18 |arkiv-dato=2018-06-18 |arkiv-url=https://web.archive.org/web/20180618130046/http://www.akershus.no/om-fylkeskommunen/om-akershus/kommuner-og-regioner/ |url-status=død }}
            {{Kilde bok| utgivelsesår = 1979 | tittel = Afrika | isbn = 8251607450 | utgivelsessted = Oslo | forlag = Schibsted | url = http://urn.nb.no/URN:NBN:no-nb_digibok_2007062004039 | side = }}
            {{Kilde artikkel| tittel=Alan Turing: Father of the Modern Computer|publikasjon=[[The Rutherford Journal]] |url=http://www.rutherfordjournal.org/article040101.html |redaktørfornavn=B. Jack | redaktøretternavn=Copeland | ref=harv}}
            {{Kilde artikkel|etternavn=Hale|fornavn=George E.|publikasjon=Popular Astronomy|år=1916|tittel=Address at the semi-centennial of the Dearborn Observatory: Some Reflections on the Progress of Astrophysics|bind=24|språk=engelsk|bibcode=1916PA.....24..550H|side=550–558, på s. 555|ref=Hale1916}}
            {{Kilde avis| url=http://www.abs-cbnnews.com/lifestyle/03/24/12/dlsu-host-intl-summit-philosophy | tittel=DLSU to host int'l summit on philosophy | verk=ABS-CBN.com | dato=24. mars 2012 | besøksdato=18. desember 2013 }}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'no')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})

    def test_translate_pl(self):
        mwtext = """
            {{cytuj stronę | url = http://www.dreamcharleston.com/charleston-history.html | tytuł= An Overview History of Charleston S.C.| opublikowany = Dream, Charleston, SC | język = en | data dostępu = 2018-01-18}}
            {{cytuj | autor = Mateusz Tałanda | tytuł = Cretaceous roots of the amphisbaenian lizards | czasopismo = Zoologica Scripta | data = 2016-01 | data dostępu = 2022-07-06 | wolumin = 45 | numer = 1 | s = 1–8 | doi = 10.1111/zsc.12138 | język = en | dostęp = z}}
            {{Cytuj pismo |tytuł=Low-Level Nuclear Activity in Nearby Spiral Galaxies |autor=Himel Ghosh, Smita Mathur, Fabrizio Fiore, Laura Ferrarese |arxiv=0801.4382v2 |czasopismo=arXiv + The Astrophysical Journal  |data=2008-06-24 |język=en |data dostępu=2012-12-13 |doi=10.1086/591508 |wolumin=687 |wydanie=1 |strony=216-229}}
            {{cytuj książkę\n | autor= Kasjusz Dion Kokcejanus\n | autor link= Kasjusz Dion\n | tytuł= Historia\n | url=  http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Cassius_Dio/home.html\n | rozdział= Księga LIII 1; LIV 6\n}}
                """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'pl')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})

    def test_translate_pt(self):
        mwtext = """
            {{Citar web|ultimo=Ohnezeit|primeiro=Maik|url=https://www.bismarck-stiftung.de/2020/10/05/kalenderblatt-der-sieges-einzug-in-paris-am-1-maerz-1871/|titulo=Kalenderblatt: „Der Sieges-Einzug in Paris am 1. März 1871“|data=2020-10-05|acessodata=2022-12-03|website=Otto-von-Bismarck-Stiftung|lingua=de-DE-formal}}
            {{Citar livro|url=https://books.google.com/books?id=04mJJlND1ccC|título=The Suppression of the African Slave-trade to the United States of America, 1638-1870|ultimo=Bois|primeiro=William Edward Burghardt Du|data=1904|editora=Longmans, Green and Company|lingua=en}}
            {{Citar livro|url=https://books.google.com/books?id=04mJJlND1ccC|título=The Suppression of the African Slave-trade to the United States of America, 1638-1870|ultimo=Bois|primeiro=William Edward Burghardt Du|data=1904|editora=Longmans, Green and Company|lingua=en}}
            {{Link|en|2=http://www.nytimes.com/learning/general/onthisday/20070309.html |3=''The New York Times'': On This Day}}
            {{Link||2=http://www.imdb.com/title/tt0384994|3=''Hidden in Plain Sight'' (2003)|4= (entrevistado)}}
            {{Citar periódico |url=https://www.bbc.com/news/world-middle-east-23700663 |título=Egypt declares national emergency |data=2013-08-14 |acessodata=2023-07-26 |periódico=BBC News |lingua=en-GB}}
            {{citar jornal|url=https://www.afro.who.int/news/africa-eradicates-wild-poliovirus|título=Africa eradicates wild poliovirus|data=25 de agosto de 2020|acessodata=25 de agosto de 2020|língua=en|agência=OMS}}
            {{link|en|http://guindo.pntic.mec.es/~jmag0042/alphaspa.html| Tipo de letra que inclui os sinais da Linear B}}
            {{Link||2=http://antena.indice.googlepages.com/home|3=antena.indice.googlepages.com/home}}
            {{Citar livro |último= Lorell |primeiro= Mark A |ano= 2002 |título= Going Global? US Government Policy and the Defense Aerospace Industry |número= 1537 |local= Santa Monica, California |publicado= RAND |ISBN= 0-8330-3193-7 |ref= harv}}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'pt')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})
            if 'isbn' in tpl.lower():
                print("Citation with ISBN, ID_lst must be present!")
                self.assertIn('ID_list', parsed)

    def test_translate_ru(self):
        mwtext = """
            {{статья|автор=[[Гейзер, Матвей Моисеевич|Гейзер М.]]|заглавие=Расставание с прошлым неизбежно|ссылка=http://www.lechaim.ru/ARHIV/112/geyzer.htm|издание=[[Лехаим (журнал)|Лехаим]]|тип=журнал|место=М.|издательство=Лехаим|год=2001|месяц=8|номер=8 (112)|issn=0869-5792|archivedate=2016-10-18|archiveurl=https://web.archive.org/web/20161018173007/http://www.lechaim.ru/ARHIV/112/geyzer.htm}}
            {{citation | last = Andriollo | first = Lisa | chapter = Les Kourkouas (IXe-XIe siècle) | language = fr | title = Studies in Byzantine Sigillography | volume = 11 | editor1-last = Cheynet | editor1-first=Jean-Claude | editor2-last = Sode | editor2-first = Claudia | location = Berlin | publisher = De Gruyter | year = 2012 | pages = 57–88 | isbn = 978-3-11-026668-9 | chapter-url = https://www.academia.edu/2074096}}
            {{книга|автор=[[Васильев, Александр Александрович (историк)|Васильев А. А.]]|заглавие=История Византийской империи |ссылка часть= http://www.hrono.ru/libris/lib_we/vaa100.php |часть=Том 1 |место=М.|издательство=Алетейя|год=2000|страниц=|isbn=978-5-403-01726-8}}
            {{Фильм\n| РусНаз           = Восемь с половиной\n| Изображение      = 8Mezzo.jpg\n| Размер           = \n| Жанр             = [[трагикомедия]]\n| Режиссёр         = [[Феллини, Федерико|Федерико Феллини]]\n| В главных ролях  = [[Мастроянни, Марчелло|Марчелло Мастроянни]]\n| Продюсер         = [[Анжело Риццоли]]\n| Сценарист        = \n| Композитор       = \n| Художник-постановщик = Пьеро Герарди\n| Компания         = {{нп4|Cineriz}}<br>Francinex\n| Оператор         = [[Ди Венанцо, Джанни|Джанни Ди Венанцо]]\n| Время            = 138 мин\n| Бюджет           = \n| Сборы            = \n|| Язык             = \n| Год              = 1963\n}}
            {{книга\n |автор         = Бергман И.\n |заглавие      = Картины\n |ссылка        = \n |ответственный = Перевод со шведского А. Афиногеновой\n |место         = М.- Таллин\n |издательство  = Музей кино, Aleksandra\n |год           = 1997\n |страницы      = \n |страниц       = 440\n |isbn          = 9985-827-27-9\n}}
            {{Телефильм\n| РусНаз      = Огненный сезон\n| ОригНаз     = The Burning Season\n| Изображение = Medium burning.jpg\n| Жанр        = [[Драма (жанр)|драма]]<br>[[фильм-биография]]\n| Режиссёр    = [[Франкенхаймер, Джон|Джон Франкенхаймер]]\n| Актёры      = [[Хулия, Рауль|Рауль Хулия]]<br>[[Брага, Сония|Сония Брага]]<br>[[Олмос, Эдвард Джеймс|Эдвард Джеймс Олмос]]\n|Продюсер = Джон Франкенхаймер<br>Томас Хэммел<br>Грасия Раде\n|<br>[[Мастросимоне, Уильям|Уильям Мастросимоне]]\n|Композитор = Гари Чанг\n|Оператор = [[Леонетти, Джон|Джон Р. Леонетти]]\n|Компания = [[HBO]]\n|Бюджет =\n}}
            {{Wayback|url=https://www.treccani.it/enciclopedia/giovanni-battista-pergolesi_(Dizionario-Biografico) |date=20201211193654 }}
            {{Wayback|url=http://s-marshak.ru/articles/andronikov.htm |date=20161031200936 }}
            {{книга|год=1980 |заглавие=When The Lights Go Down |издательство={{Нп3|Henry Holt and Company|Henry Holt & Co.|en|Henry Holt and Company}} |isbn=0-03-042511-5 |ref=Kael |язык=en |автор=Kael, Pauline}}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            # нп2, нп3, нп4 etc are Russian language templates that mean "непереведено" - untranslated
            if tpl.name.startswith('нп'):
                print("Skipping: ", tpl)
                continue
            parsed = parse_citation_template(tpl, 'ru')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})

    def test_translate_sv(self):
        mwtext = """
            {{webbref |url= http://www.catalogueoflife.org/annual-checklist/2011/search/all/key/omocestus+femoralis/match/1|titel= Species 2000 & ITIS Catalogue of Life: 2011 Annual Checklist.|hämtdatum= 24 september 2012 |författare= Bisby F.A., Roskov Y.R., Orrell T.M., Nicolson D., Paglinawan L.E., Bailly N., Kirk P.M., Bourgoin T., Baillargeon G., Ouvrard D. (red.)|datum= 2011|verk= |utgivare=Species 2000: Reading, UK.}}
            {{webref|url=https://www.phare-de-cordouan.fr/en/discover/life-at-the-lighthouse/|titel=Life at the lighthouse|språk=engelska|hämtdatum=2021-10-04}}
            {{bokref\n|titel=USA under Bill Clinton\n|serie=Utblick (Stockholm. 1981), 0280-4565 ; 1993:1\n|utgivare=Utrikespolitiska institutet\n|utgivningsort=Stockholm\n|språk=swe\n|libris=1537255\n}}
            {{Tidningsref |författare= |rubrik=Teaterannons |url=https://arkivet.dn.se/tidning/1966-01-08/6/25 |tidning=Dagens Nyheter |id= |datum=8 januari 1966 |sid=25 |hämtdatum=8 januari 2022 |språk= |arkivurl= |arkivdatum= }}
            {{tidskriftsref|författare=Stenberg L.| rubrik = Baltnycklar i Norrbotten | tidskrift = Svensk Botanisk Tidsskrift | sid = 67-69 | volym = 99 | nummer = 2 | år = 2005 | }}
        """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'sv')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})

    def test_translate_tr(self):
        mwtext = """
        {{Haber kaynağı|soyadı=Wang|ad=Guanqun|başlık=Death toll from NW China mudslides rises to 1,434|url=http://news.xinhuanet.com/english2010/china/2010-08/21/c_13455846.htm|erişimtarihi=22 Ağustos 2010|gazete=Xinhua|tarih=21 Ağustos 2010|arşivurl=https://web.archive.org/web/20100827025742/http://news.xinhuanet.com/english2010/china/2010-08/21/c_13455846.htm|arşivtarihi=27 Ağustos 2010|ölüurl=hayır}}
        {{Web kaynağı | url = http://rapory.tuik.gov.tr/13-04-2020-14:15:47-538582699815508137971101142.html? | başlık = İllere göre ilçe, bucak,belde ve köy nüfusları - 2000 | erişimtarihi = 13 Nisan 2020 | yayımcı = TÜİK | dil = Türkçe | arşivurl = https://archive.today/20200413111716/http://rapory.tuik.gov.tr/13-04-2020-14:15:47-538582699815508137971101142.html? | arşivtarihi = 13 Nisan 2020 | ölüurl = evet }}
        {{Web kaynağı | url = https://www.sehirhafizasi.sakarya.edu.tr/wp-content/uploads/2018/05/1970-n%C3%BCfus-say%C3%ACm%C3%AC.pdf | başlık = 1970 Genel Nüfus Sayımı İdari Bölünüş | tarih = 1973 | yazar = Başbakanlık Devlet İstatistik Enstitüsü | erişimtarihi = 18 Şubat 2020 | yayımcı = sehirhafizasi.sakarya.edu.tr | dil = Türkçe | arşivurl = https://web.archive.org/web/20200218084704/https://www.sehirhafizasi.sakarya.edu.tr/wp-content/uploads/2018/05/1970-n%C3%BCfus-say%C3%ACm%C3%AC.pdf | arşivtarihi = 18 Şubat 2020 | ölüurl = evet }}
        {{kitap kaynağı|ad1=Necati |soyadı1=Yeniel |ad2=Hüseyin |soyadı2=Kayapınar |başlık=Sünen-i Ebu Davud Terceme ve Şerhi |cilt=2 |sayfa=112}}
        {{dergi kaynağı|soyadı=Ertem|ad=Barış|başlık=Atatürk'ün Balkan Politikası ve Atatürk Dönemi'nde Türkiye Balkan Devletleri İlişkileri|dergi=Akademik Bakış|tarih=Temmuz-Ağustos-Eylül 2010|sayı=21|sayfa=13|yayıncı=Türk Dünyası Kırgız-Türk Sosyal Bilimler Enstitüsü|yer=İstanbul|issn=1694-528X}}
        {{dergi kaynağı|soyadı=Özgiray|ad=Ahmet|başlık=Türkiye-Mısır Siyasi İlişkileri (1920-1938)|dergi=Tarih İncelemeleri Dergisi|yıl=1996|sayı=11|sayfa=7|yayıncı=Ege Üniversitesi Edebiyat Fakültesi Tarih Bölümü|yer=İzmir|issn=0257-4152}}
        {{Webarşiv|url=https://web.archive.org/web/20200710034633/http://media.guardian.co.uk/broadcast/story/0,,1802058,00.html |tarih=10 Temmuz 2020 }}
        {{webarşiv|url=https://web.archive.org/web/20080610191343/http://www.washingtonpost.com/ac2/wp-dyn/A14897-2004Oct7?language=printer |tarih=10 Haziran 2008 }}
        {{Kitap kaynağı|soyadı=Duffy|ad=James P.|başlık=Target: America: Hitler\'s Plan to Attack the United States|yıl=2004|yayıncı=The Lyons Press|yer=Guilford, CT, USA|isbn=1-59228-934-7|sayfalar=58|url=http://books.google.com/books?id=nVdWrzRIrQ4C&pg=PA58|alıntı="Tank would become one of the world\'s leading aircraft designers and engineers."|erişimtarihi=3 Ocak 2014|arşivurl=https://web.archive.org/web/20140103145201/http://books.google.com/books?id=nVdWrzRIrQ4C&pg=PA58|arşivtarihi=3 Ocak 2014|ölüurl=hayır}}
        {{Kitap kaynağı|url=https://archive.org/details/literatureofanci0000unse_t7d8|başlık=The Literature of Ancient Sumer|bölüm=Introduction|tarih=25 Kasım 2004 |dil=İngilizce|yayıncı=OUP Oxford|isbn=978-0-19-155572-5|ad2=Graham|ad3=Eleanor|ad4=Gábor|soyadı2=Cunningham|soyadı3=Robson|soyadı4=Zólyomi|ad1=Jeremy|soyadı1=Black}}
       """
        wikicode = mwparserfromhell.parse(mwtext)
        for tpl in wikicode.filter_templates():
            parsed = parse_citation_template(tpl, 'tr')
            print(parsed)
            self.assertIsInstance(parsed, dict)
            self.assertNotEqual(parsed, {})


if __name__ == '__main__':
        unittest.main()
