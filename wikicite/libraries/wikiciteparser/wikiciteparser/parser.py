from __future__ import unicode_literals
import re
import os
import lupa
import mwparserfromhell
import importlib

lua = lupa.LuaRuntime()
lua_data_path = os.path.dirname(__file__) + os.sep
print("Lua translator looking for data files in the following directory: ", lua_data_path)

lua_parser_path = os.path.join(os.path.dirname(__file__), 'cs1.lua')
with open(lua_parser_path, 'r', encoding='utf-8') as f:
    lua_parser = f.read()

lua_translator_path = os.path.join(os.path.dirname(__file__), 'cs1-translator.lua')
with open(lua_translator_path, 'r', encoding='utf-8') as f:
    lua_translator = f.read()


# MediaWiki utilities simulated by Python wrappers
def lua_to_python_re(regex):
    rx = re.sub('%a', '[a-zA-Z]', regex) # letters
    rx = re.sub('%c', '[\x7f\x80]', regex) # control chars
    rx = re.sub('%d', '[0-9]', rx) # digits
    rx = re.sub('%l', '[a-z]', rx) # lowercase letters
    rx = re.sub('%p', '\\p{P}', rx) # punctuation chars
    rx = re.sub('%s', '\\s', rx) # space chars
    rx = re.sub('%u', '[A-Z]', rx) # uppercase chars
    rx = re.sub('%w', '\\w', rx) # alphanumeric chars
    rx = re.sub('%x', '[0-9A-F]', rx) # hexa chars
    return rx


def ustring_match(string, regex):
    return re.match(lua_to_python_re(regex), string) is not None


def ustring_len(string):
    return len(string)


def uri_encode(string):
    return string


def text_split(string, pattern):
    return lua.table_from(re.split(lua_to_python_re(pattern), string))


def nowiki(string):
    try:
        wikicode = mwparserfromhell.parse(string)
        return wikicode.strip_code()
    except (ValueError, mwparserfromhell.parser.ParserError):
        return string


# Conversion utilities, from lua objects to python objects
def is_int(val):
    """
    Is this lua object an integer?
    """
    try:
        x = int(val)
        return True
    except (ValueError, TypeError):
        return False


def to_py_dict(lua_val, wrapped_type):
    """
    Converts a lua dict to a Python one
    """
    wt = wrapped_type(lua_val)
    if wt == 'string':
        return nowiki(lua_val)
    elif wt == 'table':
        dct = {}
        lst = []
        for k, v in sorted(lua_val.items(), key=(lambda x: x[0])):
            vp = to_py_dict(v, wrapped_type)
            if not vp:
                continue
            if is_int(k):
                lst.append(vp)
            dct[k] = vp
        if lst:
            return lst
        return dct
    else:
        return lua_val


def parse_citation_dict(arguments, citation_type):
    """
    Parses the Wikipedia citation into a python dict.

    :param arguments: a dictionary with the arguments of the citation template
    :param citation_type: main part of the template name (e.g. '[cite] journal', 'citation', and so on)
    :returns: a dictionary used as internal representation in wikipedia for rendering and export to other formats
    """

    if isinstance(arguments, dict):
        if 'CitationClass' not in arguments:
            arguments['CitationClass'] = citation_type
        if 'id' in arguments:
            # In some languages, e.g., Finnish, the identifiers are grouped under a common name translated to 'id' column,
            # e.g., "Tunniste = ISBN XXX" now looks like "id = ISBN XXX" and should be mapped to "isbn = XXX"
            id_str = arguments.pop('id')
            if len(id_str) > 0:
                vals = id_str.split()
                if len(vals) > 1:
                    arguments[vals[0].lower()] = vals[1]
        if 'vauthors' in arguments:
            arguments['authors'] = arguments.pop('vauthors')
        if 'veditors' in arguments:
            arguments['editors'] = arguments.pop('veditors')
        if citation_type == "web":
            if '1' in arguments:
                arguments['URL'] = arguments.pop('1')
            if '2' in arguments:
                arguments['Title'] = arguments.pop('2')
    else:
        if len(arguments) > 0:
            if citation_type == "web":
                arguments = {'CitationClass': citation_type,
                             'URL': arguments[0],
                             'title': arguments[1] if len(arguments) > 1 else ""}
            if citation_type == "link":
                base = 1 if len(arguments[0]) < 3 else 0
                arguments = {'CitationClass': citation_type,
                             'URL': arguments[base] if len(arguments) > base else "",
                             'title': arguments[base+1] if len(arguments) > (base+1) else ""}
        else:
            print(citation_type, type(arguments), arguments)

    try:
        lua_table = lua.table_from(arguments)
        lua_result = lua.eval(lua_parser)(lua_table,
                                          ustring_match,
                                          ustring_len,
                                          uri_encode,
                                          text_split,
                                          nowiki)
    except Exception as e:
        print("Error in calling Lua code from parser: ", citation_type, arguments)
        return {'Title': 'Citation generic template not possible'}

    return to_py_dict(lua_result, lua.globals().type)


def params_to_dict(params):
    """
    Converts the parameters of a mwparserfromhell template to a dictionary
    """
    dct = {}
    for param in params:
        dct[param.name.strip().lower()] = param.value.strip()
    return dct


def translate_citation(arguments, citation_type, lang):
    """
    Translates citation to English
    """
    # print("Original arguments:", arguments)
    try:
        lua_table = lua.table_from(arguments)
        lua_result = lua.eval(lua_translator)(lua_table, lang, citation_type, lua_data_path)
    except Exception as e:
        print("Error in calling Lua code from translator: ", citation_type, arguments)
        return {'Title': 'Citation generic template not possible'}

    return to_py_dict(lua_result, lua.globals().type)


def is_citation_template_name(template_name, lang):
    """
    Check whether template name is in the language-specific list of citation templates
    """

    lang_module = importlib.import_module('.' + lang, package='wikiciteparser')
    if template_name in lang_module.citation_template_names:
        # Lua method map fails to match templates in unicode, so we translate them ahead
        unicode_template_map = {
            # Catalan
            "ref-publicació": "cite news",
            "ref-notícia": "cite news",
            "ref-web": "cite web",
            "ref-llibre": "cite book",
            "llista d'episodis": "cite av media",
            # Polish
            "cytuj książkę": "cite book",
            "cytuj stronę": "cite web",
            # Portugese
            "citar periódico": "cite journal",
            # Spanish
            "cita publicación": "cite journal",
            # Turkish
            "kitap kaynağı": "cite book",
            "haber kaynağı": "cite news",
            "dergi kaynağı": "cite journal",
            "akademik dergi kaynağı": "cite journal",
            "web kaynağı": "cite web",
            "webarşiv": "cite web",
            # Russian
            "книга": "cite book",
            "статья": "cite journal",
            "публикация": "cite journal",
            "фильм": "cite av media",
            "телефильм": "cite av media"
        }
        if template_name in unicode_template_map:
            template_name = unicode_template_map[template_name]
        split_template_name = template_name.split(' ')
        return split_template_name[-1] if len(split_template_name) > 1 else split_template_name[0]


def parse_citation_template(template, lang='en'):
    """
    Takes a mwparserfromhell template object that represents
    a wikipedia citation, and converts it to a normalized representation
    as a dict.

    :returns: a dict representing the template, or None if the template
        provided does not represent a citation.
    """
    if not template.name:
        return {}

    template_name = template.name.strip().lower()
    template_name = template_name.replace("é", "e")

    params = None
    if lang != 'en':
        citation_type = is_citation_template_name(template_name, lang)
        if citation_type:
            params = translate_citation(params_to_dict(template.params), citation_type, lang)
    if lang == 'en' or not params:
        citation_type = is_citation_template_name(template_name, 'en')
        if citation_type:
            params = params_to_dict(template.params)

    if params:
        # print("Translated arguments:", citation_type, params)
        return parse_citation_dict(params, citation_type)
    else:
        print("Not a citation template:", lang, template_name)
        return {}

