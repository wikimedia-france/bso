# -*- encoding: utf-8 -*-

# taken from https://en.wikipedia.org/wiki/Help:citation_Style_1
citation_template_names = set([
    "article",
    "ouvrage",
    "lien web",
    "chapitre",
    "lire en ligne",
    "article encyclopédique",
    "article astronomique",
    "odnb",
    "doi",
    "bibiliographie",
    "ecrit",
    "infobox livre",
    "google livres",
    "gallica",
    ])
