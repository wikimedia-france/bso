# -*- encoding: utf-8 -*-

# taken from https://en.wikipedia.org/wiki/Help:citation_Style_1
citation_template_names = set([
    'cita libro',
    'cita pubblicazione',
    'cita web',
    'cita brevetto',
    'cita conferenza',
    'cita news'
    ])
