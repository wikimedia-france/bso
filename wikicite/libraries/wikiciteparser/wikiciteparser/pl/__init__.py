# -*- encoding: utf-8 -*-

# taken from https://en.wikipedia.org/wiki/Help:Citation_Style_1
citation_template_names = set([
    "cytuj",
    "cytuj książkę",
    "cytuj stronę",
    "cytuj pismo"
    ])
