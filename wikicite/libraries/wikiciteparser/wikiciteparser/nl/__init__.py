# -*- encoding: utf-8 -*-

# taken from https://en.wikipedia.org/wiki/Help:citation_Style_1
citation_template_names = set([
    'citeer',
    'citeer boek',
    'citeer journal',
    'citeer web'
    ])
