# -*- encoding: utf-8 -*-

# taken from https://en.wikipedia.org/wiki/Help:citation_Style_1
citation_template_names = set([
    "ref-publicació",
    "ref-notícia",
    "ref-web",
    "ref-llibre",
    "citar ref",
    "llista d'episodis"
    ])
