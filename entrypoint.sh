#!/bin/bash

# Activate the virtual environment
source /app/venv/bin/activate

echo "Running $1"

# Check if JAVA_HOME is correctly set
echo "Using JAVA_HOME=$JAVA_HOME"
java -version

set -e

case "$1" in
  extract_citations)
    shift
    python3 scripts/1_extract_citations.py "$@"
    ;;
  retrieve_citation_date)
    shift
    python3 scripts/2_retrieve_citation_date.py "$@"
    ;;
  enrich_data)
    python3 scripts/3_enrich_data.py
    ;;
  visualize_data)
    python3 scripts/4_visualize_data.py
    ;;
  *)
    echo "Usage: $0 {extract_citations|retrieve_citation_date|enrich_data|visualize_data}"
    exit 1
    ;;
esac