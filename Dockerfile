# Use OpenJDK base image with Python 3.8
FROM python:3.8-slim

# Install system dependencies : OpenJDK, wget, tar
RUN apt-get update \
  && apt-get install -y --no-install-recommends default-jre wget tar \
  && apt-get autoremove -yqq --purge \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Install Spark
RUN wget https://dlcdn.apache.org/spark/spark-3.5.4/spark-3.5.4-bin-hadoop3.tgz && \
    tar -xvf spark-3.5.4-bin-hadoop3.tgz && \
    mv spark-3.5.4-bin-hadoop3 /opt/spark && \
    rm spark-3.5.4-bin-hadoop3.tgz

# Set environment variables for Spark and Java
ENV SPARK_HOME=/opt/spark
# JAVA_HOME might need to be changed depending on the default-jre installation path above
ENV JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64
ENV PATH=$JAVA_HOME/bin:$SPARK_HOME/bin:$PATH

# Set working directory
WORKDIR /app
COPY . .

# Set up virtual environment and install dependencies
RUN python3 -m venv venv && \
    . venv/bin/activate && \
    pip install --no-cache-dir -r requirements.txt && \
    pip install nltk && \
    pip install ./wikicite/libraries/wikiciteparser && \
    pip install ./wikicite/libraries/mwparserfromhell && \
    python3 -m nltk.downloader popular punkt_tab

# Default entrypoint
ENTRYPOINT ["/app/entrypoint.sh"]