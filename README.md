# BSO

L'objectif de ce projet est d'extraire les citations présentes sur le Wikipedia Francophone et Anglais, ainsi que les métadonnées associées à ces citations pouvant permettre d'identifier le statut d'ouverture ainsi que l'origine de la source de la citation.

## Architecture

Le projet se base sur des projets existants pour l'extraction des données et la génération des fichiers de sortie :

- [mwparserfromhell](https://github.com/earwig/mwparserfromhell) : Un outil permettant de parser le code wikitexte présent dans les articles Wikipedia.
- [wikiciteparser](https://github.com/dissemin/wikiciteparser) : Un outil permettant de parser les références bibliographiques présentes dans les articles Wikipedia.
- [wikicite](https://github.com/albatros13/wikicite/tree/multilang) : Un outil utilisant les deux librairies précédentes pour extraire les citations et les métadonnées associées d'un dump Wikipedia.
- [mwxml](https://github.com/mediawiki-utilities/python-mwxml) : Un outil permettant de parser les dumps Wikipedia.

Nous nous basons donc principalement sur le code existant de Wikicite, qui permet une extraction des citations et d'une partie des métadonnées associées d'un dump Wikipedia, avec un module de traduction pour une sortie homogène indépendamment de la langue du dump.

## Modification apportées

Nous avons modifié le code de Wikicite pour ajouter des fonctionnalités supplémentaires, les principales étant :

- Modification de wikiciteparser pour ajouter une liste plus complète des Templates de citations français pris en charge.
- Extraction de la métadonnée `date d'ajout` des citations afin d'avoir un historique des citations ajoutées sur Wikipedia. Cela demande d'utiliser les dumps `historiques` de Wikipedia plutôt que les dumps actuels, plus volumineux.
- Enrichissement des métadonnées des citations avec des informations externes, notamment sur le statut d'ouverture (OpenAlex, BSO).

## Installation et utilisation

Veuillez suivre la documentation présente dans le fichier [INSTALL.md](INSTALL.md)
