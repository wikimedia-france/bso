# Guide d'installation

Ce guide vous permettra de reproduire un environnement de développement local pour le projet BSO, ou un environnement de production via une image Docker.

L'équipe de Wikicite, la librairie sur laquelle ce projet est basé a également mis en place un [guide d'installation](https://github.com/albatros13/wikicite/tree/master) pour leur projet permettant de faire tourner les scripts d'extractions sur Google Cloud.

## Développement (local)

### Prérequis

Ce guide permet une installation sous Linux. Il est possible de reproduire ces étapes sous Windows via le WSL (Windows Subsystem for Linux).
Les manipulations ont été testées sur Ubuntu 20.04 et 22.04, mais devraient fonctionner sur d'autres distributions.

#### Python

[Python 3.8 ou supérieur](https://www.python.org/downloads/) est nécessaire.

#### Spark

[Spark](https://spark.apache.org/downloads.html) est nécessaire pour une extraction optimisée des dumps Wikipedia :

La version de Spark 3.5.4 a été utilisée pour le développement de ce projet, elle nécessite Java 8, 11 ou 17 : 

```bash
sudo apt install openjdk-17-jdk openjdk-17-jre
```

La variable d'environnement `JAVA_HOME` doit être configurée pour pointer vers le dossier d'installation de Java 17 :

```bash
echo "export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64" >> ~/.bashrc
source ~/.bashrc
```

Pour installer Spark, télécharger l'archive depuis le site officiel, la décompresser et la déplacer dans le dossier `/opt` :

```bash
wget https://dlcdn.apache.org/spark/spark-3.5.4/spark-3.5.4-bin-hadoop3.tgz
tar -xvf spark-3.5.4-bin-hadoop3.tgz
sudo mv spark-3.5.4-bin-hadoop3 /opt/spark
```

Puis ajouter Spark au PATH :

```bash
echo "export SPARK_HOME=/opt/spark" >> ~/.bashrc
echo "export PATH=$SPARK_HOME/bin:$PATH" >> ~/.bashrc
source ~/.bashrc
```

Vous pouvez verifier l'installation de Spark avec la commande suivante :

```bash
spark-shell
```

### Installation

Cloner le dépôt git

```bash
git clone git@framagit.org:wikimedia-france/bso.git
```

Depuis la racine du projet, créer un environnement virtuel :

```bash
python3 -m venv venv
source venv/bin/activate
```

Installer les dépendances :

```bash
pip install -r requirements.txt
```

Setup des librairies wikiciteparser et mwparserfromhell :

```bash
cd wikicite/libraries/wikiciteparser
python setup.py install

cd ../mwparserfromhell
python setup.py install
```

### Utilisation

#### <ins>Etape 1 : Extraction des citations</ins>

Vous pouvez lancer l'extraction des citations avec la commande suivante depuis la racine du projet :

```bash
python scripts/1_extract_citations.py -h
```

L'option -h permet d'afficher l'aide pour les options disponible pour cette fonction, par exemple pour extraire les citations des dumps du wikipedia francophone, à date du 2024-12-01 :

```bash
python scripts/1_extract_citations.py --lang fr --date 2024-12-01
```

Les résultats principaux seront disponibles dans le dossier `/data/parts/citations/` au format Parquet.

#### <ins>Etape 2 : Récupération de la date d'ajout des citations extraites</ins>

Pour extraire la date d'ajout des citations : 

```bash
python scripts/2_retrieve_citation_date.py -h
```

L'option -h permet d'afficher l'aide pour les options, par exemple, pour extraire les dates d'ajouts des citations extraites des dumps du wikipedia francophone, à date du 2024-12-01, et uniquement sur les dumps 1 à 5 :

```bash
python scripts/2_retrieve_citation_date.py --lang fr --date 2024-12-01 --start 1 --end 5
```

Les nouvelles colonnes seront ajoutées aux fichiers Parquet dans le dossier `/data/parts/citations/`.

#### <ins>Etape 3 : Enrichissement des métadonnées</ins>

Pour enrichir les métadonnées des citations extraites avec les sources externes (OpenAlex, BSO) :

```bash
python scripts/3_enrich_data.py
```

Les nouvelles colonnes seront ajoutées aux fichiers Parquet dans le dossier `/data/parts/citations/`.

##### <ins>Open Alex</ins>

Une clé d'authentification vers l'API d'OpenAlex peut être configurée :

```bash
cp env.template env
```
Puis modifier la valeur de la variable `OPENALEX_API_KEY`.

##### <ins>BSO</ins>

Lien vers le dump BSO : https://storage.gra.cloud.ovh.net/v1/AUTH_32c5d10cb0fe4519b957064a111717e3/bso_dump/bso-publications-latest.jsonl.gz

Ce dump est automatiquement téléchargé via le script, en cas de problème, les données peuvent-être directement placées dans le dossier data/raw.



#### <ins>Etape 4 : Visualisation des données</ins>

Une visualisation basique des données extraites est possible avec le script suivant :

```bash
python scripts/4_visualize_data.py
```

## Production (Docker)

### Docker : Prérequis

#### Docker

[Docker](https://docs.docker.com/get-docker/) est nécessaire pour lancer les scripts d'extraction dans un environnement isolé. (Docker Desktop pour Windows)

####  Docker-compose

[Docker-compose](https://docs.docker.com/compose/install/) est nécessaire pour lancer les scripts d'extraction dans un environnement isolé. (Docker Desktop pour Windows)

### Docker : Installation

Cloner le dépôt git

```bash
git clone git@framagit.org:wikimedia-france/bso.git
```

Build l'image docker :

```bash
docker-compose build
```

### Docker : Utilisation

Les scripts d'utilisation se lance de la même manière que pour l'environnement local, mais en utilisant docker-compose :

```bash
docker-compose run bso-wikicite extract_citations -h
docker-compose run bso-wikicite retrieve_citation_date -h
docker-compose run bso-wikicite enrich_data
docker-compose run bso-wikicite visualize_data
```
