from pathlib import Path
from tracemalloc import start
import requests
import pyalex
from pyalex import (
    Works,
    Authors,
    Sources,
    Institutions,
    Topics,
    Publishers,
    Funders,
    config,
)
import os
import gzip
from dotenv import dotenv_values
from pyspark.sql import SparkSession, functions as F
from pyspark.sql.functions import (
    udf,
    col,
    split,
    array_contains,
    explode,
    arrays_zip,
    lit,
    size,
    from_json,
)
from pyspark.sql.types import (
    StructType,
    StructField,
    StringType,
    ArrayType,
    IntegerType,
    MapType,
    BooleanType,
)
import pandas as pd
import json, re
from schema_utils import citations_schema, bso_schema

env_config = {
    **dotenv_values(".env.template"),  # load template variables
    **dotenv_values(".env"),  # override with user sensitive variables
    **os.environ,  # override with environment variables
}

# Load OpenAlex API key from environment variables if it has been set
if (
    "OPENALEX_API_KEY" in env_config
    and env_config["OPENALEX_API_KEY"]
    and env_config["OPENALEX_API_KEY"] != "your_open_alex_api_key"
):
    pyalex.config.api_key = env_config["OPENALEX_API_KEY"]

BSO_dump_url = "https://storage.gra.cloud.ovh.net/v1/AUTH_32c5d10cb0fe4519b957064a111717e3/bso_dump/bso-publications-latest.jsonl.gz"

# TODO : make the path configurable
citations_input_path = Path(
    "data/parts/separated/fr_citations_separatedp1p306134.parquet"
)
output_path = Path(
    "data/processed/separated/fr_citations_separatedp1p306134_enriched.parquet"
)


config.email = env_config["EMAIL"]
config.max_retries = 0
config.retry_backoff_factor = 0.1
config.retry_http_codes = [429, 500, 503]

OA_FIELDS = [
    "open_access",
    "publication_year",
    "is_core",
    "type",
    "topics",
    "is_retracted",
    "authorships",
    "primary_location",
    "language",
]

BSO_FIELDS = [
    "oa_details",
    "year",
    "journal_issns",
    "journal_name",
    "publisher_dissemination",
    "retraction_details",
    "external_ids",
    "bso_classification",
    "lang",
    "main_id",
    "id_type",
]


def select_id_from_id_list(id_list):

    priority_order = [
        "DOI",
        "PMID",
        "PMCID",
        "arXiv",
        "ISBN",
        "ISSN",
        "ISTC",
        "LCCN",
        "OCLC",
        "OL",
        "OSTI",
        "RFC",
        "URI",
        "URL",
        "URN",
        "ZDB",
        "Zotero",
    ]

    id_list = re.sub(r"'", '"', id_list)
    id_list = json.loads(id_list)
    for id_type in priority_order:
        if id_type in id_list:
            return id_type, id_list[id_type]
    return None, None


def get_oa_data(citation_identifiers):

    selected_id_type, selected_id = select_id_from_id_list(citation_identifiers)

    # print(f"Calling get_oa_data on {selected_id_type}: {selected_id}")

    citation_oa_data = {}
    for field in OA_FIELDS:
        citation_oa_data[field] = None

    try:
        # print content of works
        results = Works()[selected_id]
        with open("works.json", "w", encoding="utf-8") as f:
            json.dump(results, f, indent=4)

        citation_oa_data["open_access"] = results["open_access"]
        citation_oa_data["publication_year"] = results["publication_year"]
        # citation_oa_data["is_core"] = results["primary_location"]["is_core"] # One is_core per location, primary one should be done through primary_location
        citation_oa_data["type"] = results["type"]
        citation_oa_data["topics"] = results["topics"]
        citation_oa_data["is_retracted"] = results["is_retracted"]
        # citation_oa_data["authorships"] = results["authorships"]
        citation_oa_data["primary_location"] = results["primary_location"]
        citation_oa_data["language"] = results["language"]

        return citation_oa_data
    except:
        return {}


# Define UDF to enrich data with OpenAlex details
@udf(MapType(StringType(), StringType()))
def enrich_with_oa_data(id_list):
    oa_data = get_oa_data(id_list)
    if not isinstance(oa_data, dict):
        return {}
    return {k: str(v) if v is not None else "" for k, v in oa_data.items()}


def download_bso_dump():
    # if the file is not already downloaded, download it
    bso_dump_archive = Path("data/raw/bso-publications-latest.jsonl.gz")
    bso_dump_path = bso_dump_archive.with_suffix("")
    print(bso_dump_path)

    if not bso_dump_path.exists():

        if not bso_dump_archive.exists():
            print("Downloading BSO archive...")
            response = requests.get(BSO_dump_url)
            with open(bso_dump_path, "wb") as f:
                f.write(response.content)
            print("Download complete.")

        print("Unzipping BSO archive...")
        # using gzip to unzip the file
        with gzip.open(bso_dump_archive, "rb") as f_in:
            with open(bso_dump_path, "wb") as f_out:
                f_out.write(f_in.read())
        print("Unzip complete.")

    return bso_dump_path


# Initialize SparkSession if it's not already running
spark = SparkSession.builder.appName("LoadWikipediaCitations").getOrCreate()

spark.sparkContext.setLogLevel("ERROR")

sc = spark.sparkContext

# Read the Parquet file
df = spark.read.schema(citations_schema).parquet(str(citations_input_path))

print()
print(f"Total rows: {df.count()}")

# Only keep rows where ID_List dict is != ""
citations_with_identifier = df.filter(df.ID_List != "")

citations_with_identifier = citations_with_identifier.withColumn(
    "ID_List", from_json(col("ID_List"), MapType(StringType(), StringType()))
)


print(f"Total rows after filtering IDs: {citations_with_identifier.count()}")

# Step 1: Enrich with OpenAlex data
citations_with_identifier = citations_with_identifier.withColumn(
    "oa_data", enrich_with_oa_data(citations_with_identifier.ID_List)
)

# Extract individual fields from the OA data with prefix "oa_"
for field in OA_FIELDS:
    citations_with_identifier = citations_with_identifier.withColumn(
        f"oa_{field}", citations_with_identifier["oa_data"].getItem(field)
    )

# Drop the nested map column if no longer needed
citations_with_identifier = citations_with_identifier.drop("oa_data")

# Show the first few rows
print("OpenAlex data:")
citations_with_identifier.show()

# Step 2: Enrich with BSO dump

# download the BSO dump
bso_dump_path = download_bso_dump()

bso_df = spark.read.schema(bso_schema).json(str(bso_dump_path))

# Reduce dataframe for testing
# citations_with_identifier = citations_with_identifier.limit(100)

# Disambiguate column "id"
bso_df = bso_df.withColumnRenamed("id", "main_id")

# Extract ID_List values as a new column
citations_values = citations_with_identifier.withColumn(
    "cit_values", F.map_values(F.col("ID_List"))
)
cit_exploded = citations_values.withColumn("cit_value", F.explode("cit_values"))

# Extract external_values values as a new column
bso = bso_df.withColumn(
    "external_values", F.expr("transform(external_ids, x -> x['id_value'])")
)
bso_exploded = bso.withColumn("bso_value", F.explode("external_values"))

# Perform Join on exploded values of new columns
joined_df = cit_exploded.join(
    bso_exploded, cit_exploded.cit_value == bso_exploded.bso_value, "left"
)

# Drop intermediate columns
joined_df = joined_df.drop("cit_values", "cit_value", "external_values", "bso_value")

# Add bso_ prefix to BSO fields
for field in BSO_FIELDS:
    joined_df = joined_df.withColumn(f"bso_{field}", joined_df[field])

filtered_df = joined_df.filter(F.col("main_id").isNotNull())

# Now merge citations_with_identifier and filtered_df with "citations" and "id" columns
# Get the list of columns in both dataframes
citations_columns = set(citations_with_identifier.columns)
filtered_columns = set(filtered_df.columns)

# Identify new columns that don't already exist in citations_with_identifier
new_columns = filtered_columns - citations_columns

# Select only the new columns from filtered_df
filtered_df_new_columns = filtered_df.select(*new_columns)

# Perform the join with only the new columns
citations_with_identifier = citations_with_identifier.join(
    filtered_df_new_columns,
    (citations_with_identifier.citations == filtered_df.citations)
    & (citations_with_identifier.id == filtered_df.id),
    "left",
)

# Save as parquet file in data/processed/separated/fr_citations_separatedp1p306134.parquet
citations_with_identifier.write.mode("overwrite").parquet(str(output_path))
