import hashlib
import logging
import requests
import multiprocessing
from pathlib import Path


def parse_wikipedia_md5_list(lang, date):
    # https://dumps.wikimedia.org/frwiki/20250101/frwiki-20250101-md5sums.txt
    """
    Parse the MD5 hash list of Wikipedia dump files.

    Parameters
    ----------
    lang : str
        The language code of the Wikipedia dumps.

    date : str
        The date of the Wikipedia dumps.

    Returns
    -------
    dict
        A dictionary with the dump file name as key and the MD5 hash as value.
    """

    date = date.replace("-", "")
    md5_list_url = (
        f"https://dumps.wikimedia.org/{lang}wiki/{date}/{lang}wiki-{date}-md5sums.txt"
    )

    logging.info(f"Fetching MD5 list from {md5_list_url}...")

    try:
        md5_list = requests.get(md5_list_url).text
    except requests.RequestException as e:
        logging.critical(f"Failed to fetch MD5 list: {e}")
        return {}

    # Split the list by lines
    md5_list = md5_list.split("\n")

    # Remove empty lines if any
    md5_list = [line for line in md5_list if line]

    # Create a dictionary with the dump file name as key and the MD5 hash as value
    md5_dict = {line.split("  ")[1]: line.split("  ")[0] for line in md5_list}

    return md5_dict


def check_dump_validity(dump_name, md5_list, output_path, args):
    """
    Check the MD5 hash of a Wikipedia dump file.

    Parameters
    ----------
    dump_name : str
        The name of the dump file to find it in the list.
    md5_list : dict
        A dictionary with the dump file name as key and the MD5 hash as value.
    input_path : Path
        Path where the dump files are saved.

    Returns
    -------
    bool
        True if the file exist and the MD5 hash of the dump file matches the one from the list, False otherwise.
    """

    # First check the file exists
    dump_file_path = output_path / dump_name
    if not dump_file_path.exists():
        logging.info(f"File {dump_file_path} does not exist on disk.")
        return False

    # Retrieve the corresponding MD5 hash from the dict
    md5_hash = md5_list.get(dump_name)

    # Check the MD5 hash has been correctly retrieved and it matches the file
    if not md5_hash:
        logging.critical(f"MD5 hash not found for {dump_name}")
        logging.critical(
            "This is possibily due to the list not being up to date (e.g. dumps files are too recent)"
        )
        logging.critical(
            f"Check this URL for the corresponding list: https://dumps.wikimedia.org/{args.lang}wiki/{args.date.replace('-', '')}/"
        )
        exit(1)

    # Compute the MD5 hash of the file
    with open(dump_file_path, "rb") as f:
        file_md5 = hashlib.md5(f.read()).hexdigest()

    # Compare the MD5 hash of the file with the one from the list
    if file_md5 == md5_hash:
        logging.info(f"MD5 hash for {dump_name} is valid, skipping download.")
        return True

    logging.info(f"MD5 hash for {dump_name} is invalid.")
    # Remove the incorrect file
    dump_file_path.unlink()

    return False


def download_dump(dump_url, dump_file_path, md5_list, args):
    """
    Downloads a Wikipedia dump file.

    Parameters
    ----------
    dump_url : str
        The URL of the dump file.
    dump_file_path : Path
        The path to save the dump file.
    """

    # Check if we already have the file on disk
    is_exist = check_dump_validity(
        dump_file_path.name, md5_list, dump_file_path.parent, args
    )

    if is_exist:
        return

    logging.info(f"Downloading {dump_url}...")

    try:
        r = requests.get(dump_url, stream=True)
        r.raise_for_status()

        with open(dump_file_path, "wb") as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)
        logging.info(f"Successfully downloaded {dump_file_path}")

    except requests.RequestException as e:
        logging.error(f"Failed to download {dump_url}: {e}")


def download_all_dumps(meta_history_dumps, dumps_output_path, md5_list, args):
    """
    Download all Wikipedia dumps in parallel.

    Parameters
    ----------
    meta_history_dumps : list
        List of dump filenames.
    dumps_output_path : Path
        Path where the dumps will be saved.
    """

    dump_urls = [
        f"https://dumps.wikimedia.org/{dump_file}" for dump_file in meta_history_dumps
    ]

    dump_file_paths = [
        dumps_output_path / dump_file.split("/")[-1] for dump_file in meta_history_dumps
    ]

    # Wikipedia limits the number of simultaneous connections, so we limit the number of processes to 3
    # I.E. we download 3 dumps at a time
    with multiprocessing.Pool(processes=3) as pool:
        pool.starmap(
            download_dump,
            zip(
                dump_urls,
                dump_file_paths,
                [md5_list] * len(dump_file_paths),
                [args] * len(dump_file_paths),
            ),
        )
