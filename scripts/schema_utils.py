from pyspark.sql.types import (
    StructType,
    StructField,
    StringType,
    IntegerType,
    ArrayType,
    IntegerType,
    MapType,
    BooleanType,
)

wikidump_schema = StructType(
    [
        StructField("title", StringType(), True),
        StructField("ns", StringType(), True),
        StructField("id", StringType(), True),
        StructField("redirect", StringType(), True),
        StructField(
            "revision",
            StructType(
                [
                    StructField("id", StringType(), True),
                    StructField("parentid", StringType(), True),
                    StructField("timestamp", StringType(), True),
                    StructField(
                        "contributor",
                        StructType(
                            [
                                StructField("username", StringType(), True),
                                StructField("id", StringType(), True),
                            ]
                        ),
                        True,
                    ),
                    StructField("comment", StringType(), True),
                    StructField("text", StringType(), True),
                ]
            ),
            True,
        ),
    ]
)

citations_schema = StructType(
    [
        StructField("citations", StringType(), True),
        StructField("id", StringType(), True),
        StructField("type_of_citation", StringType(), True),
        StructField("page_title", StringType(), True),
        StructField("r_id", StringType(), True),
        StructField("r_parentid", StringType(), True),
        StructField("sections", StringType(), True),
        StructField("Degree", StringType(), True),
        StructField("City", StringType(), True),
        StructField("SeriesNumber", StringType(), True),
        StructField("AccessDate", StringType(), True),
        StructField("Chapter", StringType(), True),
        StructField("PublisherName", StringType(), True),
        StructField("Format", StringType(), True),
        StructField("Title", StringType(), True),
        StructField("URL", StringType(), True),
        StructField("Series", StringType(), True),
        StructField("Authors", StringType(), True),
        StructField("ID_List", StringType(), True),
        StructField("Encyclopedia", StringType(), True),
        StructField("Periodical", StringType(), True),
        StructField("PublicationPlace", StringType(), True),
        StructField("Date", StringType(), True),
        StructField("Edition", StringType(), True),
        StructField("Pages", StringType(), True),
        StructField("Chron", StringType(), True),
        StructField("Issue", StringType(), True),
        StructField("Volume", StringType(), True),
        StructField("TitleType", StringType(), True),
    ]
)

bso_schema = StructType(
    [
        StructField("oa_details", StringType(), True),
        StructField("year", IntegerType(), True),
        StructField("journal_issns", StringType(), True),
        StructField("journal_name", StringType(), True),
        StructField("publisher_dissemination", StringType(), True),
        StructField("retraction_details", StringType(), True),
        StructField(
            "external_ids", ArrayType(MapType(StringType(), StringType())), True
        ),
        StructField("bso_classification", StringType(), True),
        StructField("lang", StringType(), True),
        StructField("id", StringType(), True),
        StructField("id_type", StringType(), True),
    ]
)
