from ast import dump
import mwxml
import bz2
import time
import re
import os
import argparse
import logging
import multiprocessing
import numpy as np
import requests
import hashlib
from bs4 import BeautifulSoup
from datetime import datetime
import xml.etree.ElementTree as ET
from pathlib import Path
from pyspark.sql import SparkSession

from download_utils import parse_wikipedia_md5_list, download_all_dumps


def extract_page_range_from_dump_name(dump_file):

    # Extract the 'pages' portion from the file name by splitting on ".xml-" and then extracting the first part before "."
    pages_range = dump_file.split("/")[-1].split(".xml-")[1].split(".")[0]

    # Extract the first and last page numbers by splitting on 'p'
    first_page = pages_range.split("p")[1]
    last_page = pages_range.split("p")[2]

    return int(first_page), int(last_page)


def citation_in_text(citation, text):
    """Check if a citation is found in the text of a revision

    Parameters
    ----------
    citation : str
        The citation to search for
    text : str
        The text of the revision

    Returns
    -------
    bool
        True if the citation is found in the text, False otherwise
    """

    if not text:
        return False

    # TODO be a little more flexible with the citation detection :
    # If not found directly, search for the stable identifier (ex: DOI) in the text
    # If not found, search for the title of the work in the text

    # remove first and last '"' if present
    citation_raw = citation["citations"]
    if citation_raw[0] in ['"', "'"]:
        citation_raw = citation_raw[1:]
    if citation_raw[-1] in ['"', "'"]:
        citation_raw = citation_raw[:-1]

    if citation_raw in text:
        return True

    if citation["Title"] and len(citation["Title"]) > 10 and citation["Title"] in text:
        return True

    return False


def extract_doi(text):

    doi_regex = re.compile(r"\b(10[.][0-9]{4,}(?:[.][0-9]+)*/(?:(?!['\"&])\S)+)\b")

    # retrieve 0 or more matches of the regex
    doi_matches = doi_regex.findall(text) if text else []

    return doi_matches


def process_dump_and_check_citations(dump_file, citations_df, first_page, last_page):
    """Process a Wikipedia dump and check if citations are found in the revisions

    Parameters
    ----------
    dump_file : str
        The path to the Wikipedia dump file
    citations_df : pd.DataFrame
        The DataFrame containing the citations to search for
    """

    # check if file is already downloaded
    if not Path(dump_file).exists():
        logging.info(f"File {dump_file} does not exist on disk.")

        # download the dump file
        dump_url = f"https://dumps.wikimedia.org/{args.lang}wiki/{args.date.replace('-', '')}/{dump_file.name}"
        logging.info(f"Downloading dump file: {dump_url}...")

        r = requests.get(dump_url, stream=True)

        with open(dump_file, "wb") as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:
                    f.write(chunk)

        logging.info(f"File {dump_file} downloaded successfully.")

    else:
        logging.info(f"File {dump_file} exists on disk already.")

    with bz2.open(dump_file, "rb") as f:
        dump = mwxml.Dump.from_file(f)
        print(
            f"Processing dump: {dump.site_info.name} ({dump.site_info.dbname}) : {dump_file.name}"
        )

        start_time = time.time()
        nb_pages_processed = 0

        # add a column to citations_df to store the revision timestamp
        citations_df["r_id"] = None
        citations_df["r_user"] = None
        citations_df["r_timestamp"] = None

        pages_delta = last_page - first_page
        logging_delta = round((2 / 100) * pages_delta)

        for page in dump:
            nb_pages_processed += 1
            page_title, page_namespace, page_id = page.title, page.namespace, page.id

            # Filter citations for the current page_id
            citations_for_page = citations_df[citations_df["id"] == str(page_id)]

            if citations_for_page.empty:
                # print(f"No citations found for page {page_title} ({page_id})")
                continue

            perc_processed = round(nb_pages_processed / pages_delta * 100, 1)
            if nb_pages_processed % logging_delta == 0:
                elapsed_time = time.time() - start_time
                print(
                    f"{elapsed_time:.2f}s : {perc_processed}% {first_page}-{last_page} ({nb_pages_processed} pages)"
                )

            for revision in page:

                # Check if any citation is found in the revision text
                for _, citation in citations_for_page.iterrows():

                    if not citation["r_timestamp"] and citation_in_text(
                        citation, revision.text
                    ):
                        citations_for_page.at[citation.name, "r_id"] = revision.id
                        citations_for_page.at[citation.name, "r_user"] = revision.user
                        citations_for_page.at[citation.name, "r_timestamp"] = str(
                            revision.timestamp
                        )

            nb_found = citations_for_page[
                citations_for_page["r_timestamp"].notnull()
            ].shape[0]

            print(
                f"Found {nb_found}/{citations_for_page.shape[0]} citations in {page_title}"
            )

    return citations_df


def parse_args():

    # Create the parser
    parser = argparse.ArgumentParser(
        description="A script to retrieve all citations creation dates from wikipedia."
    )

    logging.basicConfig(
        level=logging.INFO,
        format="[%(asctime)s][PID %(process)d][%(levelname)s] - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    # add the -r/--resume parameter
    parser.add_argument(
        "-r",
        "--resume",
        type=bool,
        help="Resume the process from the last checkpoint. Default to False",
    )

    # lang parameter
    parser.add_argument(
        "-l",
        "--lang",
        type=str,
        help="The language of the Wikipedia dump. Default to 'fr'",
    )

    # date parameter
    parser.add_argument(
        "-d",
        "--date",
        type=str,
        help="The date of the Wikipedia dump. Default to 'latest'",
    )

    # start parameter
    parser.add_argument(
        "-s",
        "--start",
        type=int,
        help="The first dump to process. Default to 1",
    )

    # end parameter
    parser.add_argument(
        "-e",
        "--end",
        type=int,
        help="The last dump to process. Default to None",
    )

    # Add the --cutoff parameter
    parser.add_argument(
        "-c",
        "--cutoff",
        type=datetime.date,
        help="A date cutoff (format YYYY-MM-DD). Only revisions after this date will be processed. Default to None",
    )

    # add the --processes parameter
    parser.add_argument(
        "-p",
        "--processes",
        type=int,
        help="Number of processes to use for multiprocessing. Default to the number of CPUs available minus 1",
    )

    # add the -l/--logs parameter
    parser.add_argument(
        "-v",
        "--verbose",
        type=int,
        help="Logs level. Default to INFO",
    )

    # Parse the arguments
    args = parser.parse_args()

    if args.start and not args.end:
        logging.error("You must provide both the start and end parameters")
        exit(1)

    if args.end and not args.start:

        if args.start == 0:
            logging.error("The start parameter must be greater than 0")
            exit(1)

        logging.error("You must provide both the start and end parameters")
        exit(1)

    if args.start and args.end and args.start > args.end:
        logging.error("The start parameter must be less than the end parameter")
        exit(1)

    if not args.lang:
        args.lang = "fr"

    if not args.date:
        args.date = "latest"
    else:
        try:
            datetime.strptime(args.date, "%Y-%m-%d")
        except ValueError:
            logging.error("The date parameter must be in the format YYYY-MM-DD")
            exit(1)

    if args.cutoff:
        try:
            datetime.strptime(args.cutoff, "%Y-%m-%d")
        except ValueError:
            logging.error("The cutoff parameter must be in the format YYYY-MM-DD")
            exit(1)

    if args.verbose and args.verbose == 1:
        print("Setting logs level to DEBUG")
        logging.basicConfig(level=logging.DEBUG)
    elif args.verbose and args.verbose == 1:
        print("Setting logs level to INFO")
        logging.basicConfig(level=logging.INFO)
    elif args.verbose and args.verbose == 2:
        print("Setting logs level to WARN")
        logging.basicConfig(level=logging.WARN)
    elif args.verbose and args.verbose == 3:
        print("Setting logs level to ERROR")
        logging.basicConfig(level=logging.ERROR)
    elif args.verbose and args.verbose == 4:
        print("Setting logs level to CRITICAL")
        logging.basicConfig(level=logging.CRITICAL)

    return args


def worker_process_dump(dump_file, first_page, last_page, dumps_input_path):
    """
    Worker function that loads the relevant data (sub_df) within the worker process itself.
    """
    file_name = dump_file.split("/")[-1]
    file_path = dumps_input_path / file_name

    logging.info(f"Processing: {file_path}")

    # Load the data relevant to this dump file (filter only the pages in the range)
    # Assuming you have a way to load just the part of the dataset you need, for example:
    sub_df = load_data_range(first_page, last_page)

    # Now call the function to process the dump file and check citations
    process_dump_and_check_citations(file_path, sub_df, first_page, last_page)


def generate_args(meta_history_dumps, dumps_input_path):
    """
    Generator function to yield arguments for the worker processes.
    """
    for dump_file in meta_history_dumps:
        file_name = dump_file.split("/")[-1]

        # Extract page range from the dump file name
        first_page, last_page = extract_page_range_from_dump_name(file_name)

        yield (dump_file, first_page, last_page, dumps_input_path)

    logging.info(f"There are {len(meta_history_dumps)} dump files to process")


# Main multiprocessing setup
def process_dumps(meta_history_dumps, dumps_input_path, max_pool_size):
    # Create a multiprocessing pool with a specified pool size (e.g., 4 parallel processes)
    with multiprocessing.Pool(processes=max_pool_size) as pool:
        # Use pool.starmap to distribute the work across processes, using the generator for arguments
        pool.starmap(
            worker_process_dump, generate_args(meta_history_dumps, dumps_input_path)
        )

    logging.info("Processing finished")


def load_data_range(first_page, last_page):
    """
    Load only the subset of citations from the parquet file corresponding to first_page and last_page.
    """

    citations_input_path = Path(
        "data/parts/separated/fr_citations_separatedp1p306134.parquet"
    )

    # Initialize SparkSession if it's not already running
    spark = SparkSession.builder.appName("LoadWikipediaCitations").getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")

    # Read the Parquet file
    df = spark.read.parquet(str(citations_input_path))

    # Filter the dataframe by the page range (assuming 'id' is the column representing the page range)
    filtered_df = df.filter((df["id"] >= first_page) & (df["id"] <= last_page))

    # Convert the filtered Spark DataFrame to a Pandas DataFrame
    sub_df = filtered_df.toPandas()

    return sub_df


if __name__ == "__main__":

    dumps_input_path = Path("data/parts/dumps-history/")
    dumps_input_path.mkdir(parents=True, exist_ok=True)

    # retrieve all available dumps on disk
    disk_dump_files = list(dumps_input_path.glob("*.bz2"))
    max_pool_size = multiprocessing.cpu_count() - 1

    args = parse_args()

    if args.processes:
        max_pool_size = np.min([args.processes, max_pool_size])

    logging.info(f"Script parameters:")
    logging.info(f"- Language: {args.lang}")
    logging.info(f"- Dumps date: {args.date}")
    logging.info(
        f"- Using {max_pool_size} processes (CPUs available: {multiprocessing.cpu_count()})"
    )
    logging.info(f"- Cutoff date: {args.cutoff}")
    logging.info(f"- Start dump: {args.start}")
    logging.info(f"- End dump: {args.end}")
    logging.info(f"- Resume: {args.resume}")
    logging.info(f"- Logs level: {logging.getLogger().getEffectiveLevel()}")

    # -- Step 1 : Retrieve all available dumps on the Wikipedia page for the specified date -- #

    # retrieve all available dumps on wiki
    wiki_dumps_url = (
        f"https://dumps.wikimedia.org/{args.lang}wiki/{args.date.replace('-', '')}/"
    )

    index = requests.get(wiki_dumps_url).text
    soup_index = BeautifulSoup(index, "html.parser")
    # Find the links on the page
    dumps = [a["href"] for a in soup_index.find_all("a") if a.has_attr("href")]
    meta_history_dumps = [
        dump
        for dump in dumps
        if "pages-meta-history" in dump and ".bz2" in dump and "rss" not in dump
    ]

    if not meta_history_dumps:
        logging.error(
            f"No pages-meta-history dumps found on the '{args.lang}' Wikipedia page for date '{args.date}'"
        )
        logging.error(
            f"Please check a date on which meta-history dumps are available : https://dumps.wikimedia.org/{args.lang}wiki/"
        )
        exit(1)

    logging.info(
        f"Fetched {len(meta_history_dumps)} pages-meta-history dumps from the '{args.lang}' Wikipedia page for date '{args.date}'"
    )

    # only take the dumps between the start and end parameters
    if args.start and args.end:
        meta_history_dumps = meta_history_dumps[args.start - 1 : args.end]

    logging.info(
        f"Processing dumps {args.start} to {args.end} ({len(meta_history_dumps)} dumps)"
    )

    # -- Step 2 : Download the dumps in parallel -- #

    # Retrieve the MD5 hash list for the Wikipedia dump files
    md5_list = parse_wikipedia_md5_list(args.lang, args.date)

    logging.info("Starting download of dumps...")
    download_all_dumps(meta_history_dumps, dumps_input_path, md5_list, args)
    logging.info("Finished downloading")

    # -- Step 3 : Process the dumps in parallel -- #

    process_dumps(meta_history_dumps, dumps_input_path, max_pool_size)

    logging.info("Processing finished")
