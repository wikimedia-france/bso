from pyspark.sql import SparkSession
import time

start_time = time.time()

# Initialize SparkSession
spark = SparkSession.builder.appName("LoadMultipleParquetFiles").getOrCreate()

# Path to the directory containing the Parquet files
parquet_directory = (
    "data/processed/separated/fr_citations_separatedp1p306134_enriched.parquet"
)

# Read all Parquet files from the directory
df = spark.read.parquet(parquet_directory)

# Total rows
print(f"Total rows: {df.count()}")

# df = df.filter(df.citations.contains(r"{{Chapitre"))

# show column citations
df.select("citations").show()

# Show the first few rows
df.show()

# Show number of each values in column type_of_citation
df.groupBy("type_of_citation").count().show()

# Onl show citation where type_of_citation = doi
df.filter(df.type_of_citation == "gallica").show()

# For large datasets, you might want to perform further operations, e.g., count the rows
print(f"Total rows: {df.count()}")

print(f"Done in {time.time() - start_time} seconds.")
